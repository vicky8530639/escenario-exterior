using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interior1 : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("puertaInterior_1");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("puertaInterior_1cerrar");
    }
}
