using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puerta3 : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("puerta_3");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("puerta_3cerrar");
    }
}
