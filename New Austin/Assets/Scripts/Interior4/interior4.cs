using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interior4 : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("puertaInterior_4");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("puertaInterior_4cerrar");
    }
}
