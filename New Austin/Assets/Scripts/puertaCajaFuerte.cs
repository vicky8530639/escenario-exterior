using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puertaCajaFuerte : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("puertaCajaFuerte");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("cerrarpuertaCajaFuerte");
    }
}
